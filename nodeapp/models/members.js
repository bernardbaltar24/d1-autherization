//Declare Dependencies

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("validator");
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");


//Define your schema
//firstname, lastname, position, timestamps
const memberSchema = new Schema(
	{
		firstName:{
			type: String,
			trim: true,
			default: "J",
			maxlength: 30,
		},

		lastName:{
			type: String,
			trim: true,
			default: "Doe",
			maxlength: 30,
		},

		username: {
			type: String,
			required: true,
			trim: true,
			maxlength: 10,
			unique: true,
			validate(value){
				if(!validator.isAlphanumeric(value)){
					throw new Error("Characters must be letters and numbers only!")
				}
			}
		},

		position: {
			type:String,
			required: true,
			enum: ["instructor", "student", "hr", "admin", "ca"]
		},

		age: {
			type: Number,
			min: [18, "You must be at least 18 to join a team!"],
			required: true
		},

		email: {
			type: String,
			required: true,
			unique: true,
			trim: true,
			validate(value){
				if(!validator.isEmail(value)){
					throw new Error("Email is invalid!")
				}
			}
		},

		password:{
			type: String,
			required: true,
			minlength: [5, "Password must be at least 5 Characters!"]
		},
		tokens: [
			{
				token: {
					type: String,
					required: true
				}
			}
		]
	},
	{
		timestamps: true
	}
);
//HASH PASSWORD WHEN CREATING AND UPDATING MEMBER
memberSchema.pre("save", async function(next){
	const member = this

	if(member.isModified('password')){
		//SALT
		const salt = await bcrypt.genSalt(10);
		//HASH
		member.password = await bcrypt.hash(member.password, salt);
	}

	next();
})

//FIND BY CREDENTIALS USING STATICS
memberSchema.statics.findByCredentials = async (email, password) => {
	//check if emails exist
	// const member = await Member.findOne({ email })

	// if(!member){
	// 	throw new Error("Email is incorrect!")} //for now

	// const isMatch = await bcrypt.compare(password, member.password); //after ma findone ang email the password will compare to data base

	// if(!isMatch){
	// 	throw new Error("Incorrect password!")} //for now

	// return member
}

//GENERATE TOKENS
memberSchema.methods.generateAuthToken = async function(){
	const member = this

	//1 - data to embed
	//2 - secret message
	//3 - options
	const token = jwt.sign({_id: member._id.toString()}, "batch40", {expiresIn: '2 days'})

	//save the token to our db
	member.tokens = member.tokens.concat({token})
	await member.save();
	return token;

};

//HIDE PRIVATE DATA
memberSchema.methods.toJSON = function(){
	const member = this

	//return RAW member object with ALL its fields
	const memberObj = member.toObject();

	//delete a private data
	delete memberObj.password;

	//delete all tokens
	delete memberObj.tokens;

	return memberObj;
}

memberSchema.plugin(uniqueValidator);
//Export your model
const Member = mongoose.model("Member", memberSchema);
module.exports = Member;
