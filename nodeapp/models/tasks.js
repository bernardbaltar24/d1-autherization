//Declare Dependencies

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("validator");

//Define your schema
//description, teamId-String, isCompleted-boolean
const taskSchema = new Schema(
	{
		description: {
			type:String,
			required: true,
			maxlength: 100
		},

		teamId: {
			type: String,
			required: true
		},

		isCompleted: {
			type: Boolean,
			default: true
		}
	},
	{
		timestamps: true
	}
	);

//Export your model

module.exports = mongoose.model("Task", taskSchema);